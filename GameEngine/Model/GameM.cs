﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GameEngine.Model
{
    public class GameM
    {
        private const int TwoSecs = 2000;
        public GameBoardM GameBoard { get; set; }
        public List<PlayerM> Players { get; set; }
        public PlayerM Player { get; set; }
        public CardDeckM Deck { get; set; }

        public event MessageHandler Message;
        public event PlayerHandler PlayerSwitched;
        public event ComputerProgressHandler ComputerProgress;
        
        public GameM()
        {
            Initialize();
            ApplyEventHandlers();
            ApplyPlayers();
        }

        private async Task Pause(int ms)
        {
            await Task.Delay(ms);
        }

        private async void RaiseMessage(string message)
        {
            if (Message != null)
                Message(message);

            await Pause(TwoSecs);
        }

        private void RaiseComputerProgress(int percent)
        {
            if (ComputerProgress != null)
                ComputerProgress(percent);
        }

        private void Initialize()
        {
            Players = new List<PlayerM>();
            GameBoard = new GameBoardM();
            Deck = new CardDeckM();
        }

        private void ApplyEventHandlers()
        {
            foreach (var card in Deck.UnusedCards)
                card.Picked += CardPicked;

            foreach (var tile in GameBoard.Tiles)
                tile.Picked += TileClicked;
        }

        private void ApplyPlayers()
        {
            PlayerM humanPlayer1 = new Human(Colors.Blue, 1);
            humanPlayer1.ApplyPieces(GameBoard.BlueStartTiles);
            
            PlayerM computerPlayer2 = new Computer(Colors.Red, 2);
            computerPlayer2.ApplyPieces(GameBoard.RedStartTiles);           

            PlayerM computerPlayer3 = new Computer(Colors.Yellow,3);
            computerPlayer3.ApplyPieces(GameBoard.YellowStartTiles);

            PlayerM computerPlayer4 = new Computer(Colors.Green, 4);
            computerPlayer4.ApplyPieces(GameBoard.GreenStartTiles);

            Players.Add(humanPlayer1);
            Players.Add(computerPlayer2);
            Players.Add(computerPlayer3);
            Players.Add(computerPlayer4);

            Player = humanPlayer1;
        }

        public void StartGame()
        {
            RaiseMessage("New game");
            Player = Players[Players.Count - 1];
            EndTurn();
        }
      
        private void TileClicked(TileM tile)
        {
            if(!(Player is Human))
            {
                RaiseMessage("It's not your turn");
                return;
            }
                
            PieceM piece = TileToPiece(tile);

            if (piece != null)
            {
                var card = TileToCard(tile);
                Move(piece, tile, card);
            }

            else
                RaiseMessage("Not possible");
        }

        private PieceM TileToPiece(TileM tile)
        {
            return Player.Pieces.FirstOrDefault(piece => piece.ReachableTiles.Contains(tile));
        }

        private void CardPicked(CardM card)
        {
            var tiles = CardToTilesAllPieces(card);

            foreach (var tile in tiles)
                tile.IsMarked = true;
        }

        private void ApplyRachableTilestoPieces()
        {
            foreach (PieceM piece in Player.Pieces)
            {
                piece.ReachableTiles.Clear();

                foreach (CardM card in Player.Cards)
                    piece.ReachableTiles = piece.ReachableTiles.Concat(CardToTilesOnePieces(card, piece.Tile)).ToList();
            }
        }

        private async void Move(PieceM piece, TileM tile, CardM card)
        {
            if (Player is Computer)
                await SimulateComputerProgress();            

            if (tile.HasPieces && tile.PieceOnTileColor != piece.Color)
            {
                RaiseMessage(ColorToName(tile.Pieces[0].Color) + " got kicked");
                tile.KickPiece();
            }

            tile.Enter(piece);
            
            Player.RemoveCard(card);
            Deck.ReturnCard(card);
            await Pause(TwoSecs);
            EndTurn();
        }

        private CardM TileToCard(TileM tile)
        {
            return (from card in Player.Cards let possibleTiles = CardToTilesAllPieces(card) where possibleTiles.Contains(tile) select card).FirstOrDefault();
        }

        private void DrawCards()
        {
            foreach (var player in Players)
            {
                for (int i = 0; i < 4; i++)
                {
                    var card = Deck.DrawCard();
                    player.AddCard(card);
                }    
            }
        }

        public async void EndTurn()
        {
            
            Player.UnmarkReachableTiles();
            NextPlayer();

            if (IsNewRound())
            {
                DrawCards();
                RaiseMessage("New round");
            }

            ApplyRachableTilestoPieces();
            
            if (!Player.HasCards)
                EndTurn();

            else if (!Player.HasReachableTiles)
            {
                await Pause(TwoSecs);
                RaiseMessage(ColorToName(Player.Color) + " returns cards");
                ReturnAllCards();
                await Pause(TwoSecs);
                EndTurn();
            }

            else
                Play();
        }

        private void NextPlayer()
        {
            int playerIndex = Players.IndexOf(Player);

            if (playerIndex == (Players.Count - 1))
                playerIndex = 0;

            else
                playerIndex++;

            Player = Players[playerIndex];

            RaisePlayerSwitched();   
        }

        private void RaisePlayerSwitched()
        {
            if (PlayerSwitched != null)
                PlayerSwitched(Player);
        }

        private bool IsNewRound()
        {
            bool noPlayersHaveCards = true;
            foreach (PlayerM player in Players)
                noPlayersHaveCards = noPlayersHaveCards && !(player.Cards.Count > 0);

            return noPlayersHaveCards;
        }

        private async void Play()
        {
            await Pause(TwoSecs);
            if (Player is Computer)
            {
                RaiseComputerProgress(0);
                PieceM usuablePiece = null;
                TileM usuabletile = null;
                CardM usuableCard = null; 
               
                foreach (CardM card in Player.Cards)
                {
                    foreach (PieceM piece in Player.Pieces)
                    {
                        List<TileM> reachableTiles = CardToTilesOnePieces(card, piece.Tile);

                        if (reachableTiles.Count > 0)
                        {
                            usuablePiece = piece;
                            usuableCard = card;
                            usuabletile = reachableTiles[0];
                            break;
                        }
                    }
                }

                Move(usuablePiece, usuabletile, usuableCard);
            }
        }

        private async Task SimulateComputerProgress()
        {
            for (int i = 0; i < 100; i++)
            {
                RaiseComputerProgress(i);
                await Pause(30);
            }
            RaiseComputerProgress(0);
        }

        private void ReturnCard(CardM card)
        {
            Player.RemoveCard(card);
            Deck.ReturnCard(card);
        }

        private void ReturnAllCards()
        {
            var allCards = new ObservableCollection<CardM>(Player.Cards);

            foreach (CardM card in allCards)
                ReturnCard(card);
        }

        private string ColorToName(Color color)
        {
            var colors = typeof(Colors);
            foreach (var property in colors.GetProperties())
            {
                if ((Color)property.GetValue(null, null) == color)
                    return property.Name;
            }

            return "";
        }

        public List<TileM> NumericCardToTiles(TileM tile, int value)
        {
            var tiles = new List<TileM>();

            // Only start cards can unleash player
            if (tile is StartTile)
                return tiles;

            //Tile tile = boardTile as Tile;

            if (value > 0)
            {
                while (tile != null)
                {
                    if (tile is BoardTile)
                    {
                        if (((BoardTile)tile.Next).IsBlocking)
                            break;

                        tile = tile.Next;
                        value--;

                        if (value == 0)
                            break;
                    }

                    if (tile is HomeTile)
                    {
                        
                    }
                }

                if (value == 0 && tile != null)
                    tiles.Add(tile);
            }

            // Negative
            if (value < 0)
            {
                for (int i = 0; i > value; i--)
                {
                    var boardTile = (BoardTile)tile;
                    if (boardTile != null) 
                        tile = boardTile.Prev;
                }

                tiles.Add(tile);
            }

            return tiles;
        }

        public List<TileM> StartCardToTile(TileM tile)
        {
            var tiles = new List<TileM>();

            if (tile is StartTile)
                tiles.Add(tile.Next);

            return tiles;
        }

        private List<TileM> CardToTilesAllPieces(CardM card)
        {
            var allReachableTiles = new List<TileM>();

            foreach (PieceM piece in Player.Pieces)
                allReachableTiles = allReachableTiles.Concat(CardToTilesOnePieces(card, piece.Tile)).ToList();
            
            return allReachableTiles;
        }

        private List<TileM> CardToTilesOnePieces(CardM card, TileM tile)
        {           
            if (card is StartNumericCard)
            {
                List<TileM> tiles = StartCardToTile(tile);
                return tiles.Concat(NumericCardToTiles(tile, (card as StartNumericCard).Value)).ToList();
            }

            if (card is DoubleNumericCard)
            {
                List<TileM> tiles = NumericCardToTiles(tile, (card as DoubleNumericCard).Values[0]);
                return tiles.Concat(NumericCardToTiles(tile, (card as DoubleNumericCard).Values[1])).ToList();
            }

            if (card is NumericCard)
               return NumericCardToTiles(tile, (card as NumericCard).Value);

            if (card is StartCard)
                return StartCardToTile(tile);

            return new List<TileM>();
        }
    }
}

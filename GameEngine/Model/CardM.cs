﻿using System.Collections.Generic;

namespace GameEngine.Model
{
    public abstract class CardM
    {
        public event CardHandler Picked;

        public string Name { get { return GetType().ToString(); }}

        private bool _isPicked;
        public bool IsPicked
        {
            get { return _isPicked; }
            set { _isPicked = value; RaisePicked(); }
        }

        private void RaisePicked()
        {
            if (Picked != null)
                Picked(this);
        }
    }

    public class NumericCard : CardM
    {
        public int Value { get; set; }
    }

    public class DoubleNumericCard : CardM
    {
        public List<int> Values { get; set; }

        public DoubleNumericCard()
        {
            Values = new List<int>();
        }
    }

    public class StartNumericCard : NumericCard {}
    public class SplitableCard : NumericCard{}
    public class StartCard : CardM {}
    public class SwapCard : CardM{}
}

﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using GameEngine.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameEngineTest
{
    [TestClass]
    public class TileMTest
    {
        private int _eventCount;

        [TestMethod]
        public void TileIsBlocking()
        {
            var bluePiece1 = new PieceM(null,Colors.Blue) {Tile = new StartTile()};
            var tile = new BoardTile {Number = 0, Pieces = new ObservableCollection<PieceM>()};
            tile.Enter(bluePiece1);
            Assert.IsTrue(tile.IsBlocking);
        }

        [TestMethod]
        public void KickPiece()
        {
            var gameBoard = new GameBoardM();
            var bluePiece = new PieceM(null, Colors.Blue) { Tile = gameBoard.Tiles[2], HomeTile = gameBoard.BlueStartTiles[0]};
            gameBoard.Tiles[3].Enter(bluePiece);

            var yellowPiece = new PieceM(null, Colors.Yellow) {Tile = gameBoard.Tiles[52]};
            gameBoard.Tiles[3].Enter(yellowPiece);
            gameBoard.Tiles[3].KickPiece();

            Assert.IsTrue(bluePiece.Tile == gameBoard.BlueStartTiles[0]);
        }

        [TestMethod]
        public void TileEventsRaised()
        {
            var tile = new BoardTile();
            var piece = new PieceM(null, Colors.Blue) { Tile = new StartTile() };
            
            tile.Picked += TestEventHandler;
            tile.PieceEntered += TestEventHandler2;
            tile.PieceLeft += TestEventHandler3;

            tile.Pick();
            tile.Enter(piece);
            (new BoardTile()).Enter(piece);
            Assert.AreEqual(3,_eventCount);
        }

        private void TestEventHandler3(PieceM piece)
        {
            _eventCount++;
        }

        private void TestEventHandler2(PieceM piece)
        {
            _eventCount++;
        }

        private void TestEventHandler(TileM tile)
        {
            _eventCount++;
        }
    }
}

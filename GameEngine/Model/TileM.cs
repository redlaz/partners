﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;

namespace GameEngine.Model
{
    public abstract class TileM
    {
        protected const int BlueStart = 0;
        protected const int RedStart = 15;
        protected const int YellowStart = 30;
        protected const int GreenStart = 45;

        public event TileHandler Marked;
        public event TileHandler Picked;
        public event PieceHandler PieceEntered;
        public event PieceHandler PieceLeft;
        
        public ObservableCollection<PieceM> Pieces { get; set; }
        public TileM Next { get; set; }
        public bool HasPieces { get { return Pieces.Count > 0; } }
        public Color PieceOnTileColor { get { return Pieces[0].Color; } }

        private bool _isMarked;
        public bool IsMarked
        {
            get { return _isMarked; }
            set { _isMarked = value; RaiseMarked(); }
        }

        private void RaiseMarked()
        {
            if (Marked != null)
                Marked(this);
        }

        private void RaisePieceEntered(PieceM piece)
        {
            if (PieceEntered != null)
                PieceEntered(piece);
        }

        private void RaisePieceLeft(PieceM piece)
        {
            if (PieceLeft != null)
                PieceLeft(piece);
        }

        private void RaisePicked()
        {
            if (Picked != null)
                Picked(this);
        }

        public void Enter(PieceM piece)
        {
            piece.Tile.Leave(piece);
            Pieces.Add(piece);
            piece.Tile = this;
            RaisePieceEntered(piece);
        }

        private void Leave(PieceM piece)
        {
            Pieces.Remove(piece);
            RaisePieceLeft(piece);
        }
        
        public TileM()
        {
            Pieces = new ObservableCollection<PieceM>();
        }

        public void Pick()
        {
            RaisePicked();
        }

        public void KickPiece()
        {
            if (HasPieces)
                Pieces.First().GoToHome();
        }
    }

    public class StartTile : TileM {}

    public class HomeTile : TileM {}

    public class BoardTile : TileM
    {
        public int Number { get; set; }
        public TileM Prev { get; set; }
        public TileM HomeEntrance { get; set; }

        public bool IsBlocking 
        {
            get
            {
                if (HasPieces)
                { 
                    if (Number == BlueStart && PieceOnTileColor == Colors.Blue)
                        return true;

                    if (Number == RedStart && PieceOnTileColor == Colors.Red)
                        return true;

                    if (Number == YellowStart && PieceOnTileColor == Colors.Yellow)
                        return true;

                    if (Number == GreenStart && PieceOnTileColor == Colors.Green)
                        return true;
                }

                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameControls
{
	/// <summary>
	/// Interaction logic for Piece.xaml
	/// </summary>
	public partial class Piece : UserControl
	{
        // Color property binding to piece fill
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string), typeof(Piece), null);

        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

		public Piece()
		{
			this.InitializeComponent();
		}

        public static Piece Blue()
        {
            Piece piece = new Piece();
            piece.Color = "blue";
            return piece;
        }
	}
}
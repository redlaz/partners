﻿using System.Windows;
using System.Windows.Media;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class HomeTileVM
    {
        HomeTile _homeTile;

        public string Color { get; set; }
        public string HorizontalAlignment { get; set; }
        public string VerticalAlignment { get; set; }
        public Thickness Margin { get; set; }


        public HomeTileVM(HomeTile homeTile, Color color)
        {
            _homeTile = homeTile;
            Color = color.ToString();
            HorizontalAlignment = "Left";
            VerticalAlignment = "Top";
        }
    }
}

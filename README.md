# **Partners** #

C# implementation of the classic board game. Graphical application developed with C# and WPF. Built on the MVVM pattern

**Uses:**

* ItemsControl
* ContentControl
* DataTemplates

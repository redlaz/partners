﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class GameBoardVM
    {
        private readonly GameBoardM _gameBoard;

        public double Width { get; set; }
        public Point Center { get; set; }      
        
        // Board tiles
        public List<BoardTileVM> Tiles { get; set; }
       
        // Homes
        public HomeVM BlueHome { get; set; }
        public HomeVM RedHome { get; set; }
        public HomeVM YellowHome { get; set; }
        public HomeVM GreenHome { get; set; }

        // Starts
        public StartVM BlueStart { get; set; }
        public StartVM RedStart { get; set; }
        public StartVM YellowStart { get; set; }
        public StartVM GreenStart { get; set; }
        
        public GameBoardVM(GameBoardM gameBoard)
        {
            _gameBoard = gameBoard;
            Initialize();

            ApplyBoardTiles();
            ApplyHomes();
            ApplyStarts();
        }

        private void ApplyStarts()
        {
            BlueStart = new StartVM(Colors.Blue, _gameBoard.BlueStartTiles);
            RedStart = new StartVM(Colors.Red, _gameBoard.RedStartTiles);
            YellowStart = new StartVM(Colors.Yellow, _gameBoard.YellowStartTiles);
            GreenStart = new StartVM(Colors.Green, _gameBoard.GreenStartTiles);
        }

        private void ApplyHomes()
        {
            BlueHome = new HomeVM(_gameBoard.BlueHomeTiles, Colors.Blue);
            RedHome = new HomeVM(_gameBoard.RedHomeTiles, Colors.Red);
            YellowHome = new HomeVM(_gameBoard.YellowHomeTiles, Colors.Yellow);
            GreenHome = new HomeVM(_gameBoard.GreenHomeTiles, Colors.Green);
        }

        private void Initialize()
        {
            Center = new Point { X = Width / 2, Y = Width / 2 };
            Tiles = new List<BoardTileVM>();
            Width = 500;
        }

        private void ApplyBoardTiles()
        {
            foreach (BoardTile tile in _gameBoard.Tiles)
	        {
                var tileVM = new BoardTileVM(tile);
                tileVM.Position = GetPosition(tile.Number, 273);
                Tiles.Add(tileVM);
	        }
        }

        private Point GetPosition(int tileNumber, int radius)
        {
            int angle = tileNumber * 6;
            var point = new Point();
            const double rad = (Math.PI / 180);
            point.X =  Math.Sin(rad * angle) * radius;
            point.Y = Math.Cos(rad * angle) * radius;
            return point;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameControls
{
	/// <summary>
	/// Interaction logic for CardStart.xaml
	/// </summary>
	public partial class GameBoardCard : Button
	{
        // Title horizontal
        public static readonly DependencyProperty TitleHorizontalProperty = DependencyProperty.Register("TitleHorizontal", typeof(string), typeof(GameBoardCard), null);
        public string TitleHorizontal
        {
            get { return (string)GetValue(TitleHorizontalProperty); }
            set { SetValue(TitleHorizontalProperty, value); }
        }

        // Title vertical
        public static readonly DependencyProperty TitleVerticalProperty = DependencyProperty.Register("TitleVertical", typeof(string), typeof(GameBoardCard), null);
        public string TitleVertical
        {
            get { return (string)GetValue(TitleVerticalProperty); }
            set { SetValue(TitleVerticalProperty, value); }
        }

        // Corners
        public static readonly DependencyProperty CornerProperty = DependencyProperty.Register("Corner", typeof(string), typeof(GameBoardCard), null);
        public string Corner
        {
            get { return (string)GetValue(CornerProperty); }
            set { SetValue(CornerProperty, value); }
        }

        // Color
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string), typeof(GameBoardCard), null);
        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

		public GameBoardCard()
		{
			this.InitializeComponent();
		}
	}
}
﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using GameEngine.Extension;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class PlayerVM
    {
        private readonly PlayerM _player;
        public ObservableCollection<CardVM> Cards { get; set; }
        public Color Color { get{return _player.Color;}}
        public int Number { get; set; }


        public PlayerVM(PlayerM player)
        {
            _player = player;
            Number = player.Number;

            ApplyEventHandlers();
            Initialize();

            foreach (CardM card in player.Cards)            
                Cards.Add(new CardVM(card));
        }

        private void ApplyEventHandlers()
        {
            _player.CardAdded += CardAdded;
            _player.CardRemoved += CardRemoved;
        }

        private void Initialize()
        {
            Cards = new ObservableCollection<CardVM>();
        }

        private void CardRemoved(CardM card)
        {
            Cards.RemoveObject(cardVM => cardVM.Card == card);
        }

        private void CardAdded(CardM card)
        {
            Cards.Add(new CardVM(card));
        }
    }
}

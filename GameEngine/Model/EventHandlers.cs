﻿using System.Collections.Generic;

namespace GameEngine.Model
{
    public delegate void CardHandler(CardM card);
    public delegate void CardsHandler(List<CardM> cards);
    public delegate void PieceHandler(PieceM piece);
    public delegate void PlayerHandler(PlayerM player);
    public delegate void MessageHandler(string message);
    public delegate void TileHandler(TileM tile);
    public delegate void ComputerProgressHandler(int percent);
}

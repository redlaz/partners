﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public enum HomeAlignment
    {
        Left, Right, Top, Bottom
    }

    public class HomeVM
    {
        private const int Offset = 146;
        public int Height { get; set; }
        public int Width { get; set; }
        public string Alignment { get; set; }
        public string HorizontalAlignment { get; set; }
        public string VerticalAlignment { get; set; }
        public Thickness Margin { get; set; }
        public ObservableCollection<HomeTileVM> Tiles { get; set; }

        public HomeVM(IEnumerable<HomeTile> homeTiles, Color color)
        {
            Initialize();

            foreach (var homeTile in homeTiles)
            {
                Tiles.Add(new HomeTileVM(homeTile,color));
            }

            if(color == Colors.Green)
            {
                Margin = new Thickness(Offset, 0, 0, 0);
                Alignment = "Horizontal";
                Width = 120;
                Height = 30;
                HorizontalAlignment = "Left";
                VerticalAlignment = "Center";
            }

            if (color == Colors.Red)
            {
                Margin = new Thickness(0, 0, Offset, 0);
                Alignment = "Horizontal";
                Width = 120;
                Height = 30;
                HorizontalAlignment = "Right";
                VerticalAlignment = "Center";
            }

            if (color == Colors.Yellow)
            {
                Margin = new Thickness(0, Offset, 0, 0);
                Alignment = "Vertical";
                Width = 30;
                Height = 120;
                HorizontalAlignment = "Center";
                VerticalAlignment = "Top";
            }

            if (color == Colors.Blue)
            {
                Margin = new Thickness(0, 0, 0, Offset);
                Alignment = "Vertical";
                Width = 30;
                Height = 120;
                HorizontalAlignment = "Center";
                VerticalAlignment = "Bottom";
            }
        }

        private void Initialize()
        {
            Tiles = new ObservableCollection<HomeTileVM>();
        }
    }
}

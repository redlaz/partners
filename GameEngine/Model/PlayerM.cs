﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace GameEngine.Model
{
    public abstract class PlayerM
    {
        public event CardHandler CardAdded;
        public event CardHandler CardRemoved;

        public ObservableCollection<CardM> Cards { get; set; }
        public List<PieceM> Pieces { get; set; }
        public bool HasCards { get { return Cards.Count > 0; } }

        public bool HasReachableTiles 
        { 
            get 
            {
                return Pieces[0].ReachableTiles.Count > 0 || Pieces[1].ReachableTiles.Count > 0 || Pieces[2].ReachableTiles.Count > 0 || Pieces[3].ReachableTiles.Count > 0;
            }         
        }

        public int Number { get; set; }

        private Color _color;
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }
        

        public PlayerM(Color color, int number)
        {
            Initialize();

            Color = color;
            Number = number;
        }      

        private void Initialize()
        {
            Pieces = new List<PieceM>();
            Cards = new ObservableCollection<CardM>();
        }

        public void RemoveCard(CardM card)
        {
            if (CardRemoved != null)
                CardRemoved(card);

            Cards.Remove(card);
        }

        public void AddCard(CardM card)
        {
            if (CardAdded != null)
                CardAdded(card);

            Cards.Add(card);
        }

        public CardM PickFirstCard()
        {
            return Cards[0];
        }

        public void ApplyPieces(List<StartTile> homeTiles)
        {
            for (int i = 0; i < 4; i++)
            {
                var piece = new PieceM(homeTiles[i], Color);
                homeTiles[i].Pieces.Add(piece);
                Pieces.Add(piece);
            }
        }

        public void UnmarkReachableTiles()
        {
            foreach (var piece in Pieces)
            {
                foreach (var tile in piece.ReachableTiles)
                    tile.IsMarked = false;
            }
        }
    }

    [Serializable]
    public class Human : PlayerM
    { 
        public Human(Color color, int number) : base(color, number) {}
    }

    [Serializable]
    public class Computer : PlayerM
    {
        public Computer(Color color, int number) : base(color, number){}
    }
}


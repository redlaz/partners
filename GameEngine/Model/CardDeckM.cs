﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using GameEngine.Exception;
using GameEngine.Extension;

namespace GameEngine.Model
{
    public class CardDeckM
    {
        private const int NumOfCardGroup = 4;
        private const int NumOfCards = 52;

        public event CardHandler CardDrawn;
        public event CardHandler CardReturned;
        public event CardsHandler DeckShuffled;

        public List<CardM> UnusedCards { get; set; }
        public List<CardM> UsedCards { get; set; }

        public CardDeckM()
        {
            Initialize();
            CreateCards();
        }

        private void Initialize()
        {
            UnusedCards = new List<CardM>();
            UsedCards = new List<CardM>();
        }

        private void CreateCards()
        {
            for (int i = 0; i < NumOfCardGroup; i++)
            { 
                UnusedCards.Add(new NumericCard { Value = 2 });
                UnusedCards.Add(new NumericCard { Value = 3 });
                UnusedCards.Add(new NumericCard { Value = 5 });
                UnusedCards.Add(new NumericCard { Value = 9 });
                UnusedCards.Add(new NumericCard { Value = 10 });
                UnusedCards.Add(new NumericCard { Value = 12 });
                UnusedCards.Add(new NumericCard { Value = -4 });
                UnusedCards.Add(new DoubleNumericCard { Values = new List<int> { 1, 14 } });
                UnusedCards.Add(new DoubleNumericCard { Values = new List<int> { 1, 14 } });
                UnusedCards.Add(new StartNumericCard { Value = 13 });
                UnusedCards.Add(new StartNumericCard { Value = 8 });
                UnusedCards.Add(new StartCard());
            }

            UnusedCards.Shuffle();
        }

        public CardM DrawCard()
        {
            if (UnusedCards.Count == 0)
                throw new DeckEmptyException();

            CardM card = UnusedCards[0];
            UnusedCards.Remove(card);

            if (CardDrawn != null)
                CardDrawn(card);

            return card;
        }

        public void ReturnCard(CardM card)
        {
            UsedCards.Add(card);

            if (UnusedCards.Count == 0 && UsedCards.Count == NumOfCards)
                ShuffleCards();

            if (CardReturned != null)
                CardReturned(card);
        }

        private void ShuffleCards()
        {
            var newCardDeck = new ObservableCollection<CardM>(UsedCards);

            UsedCards.Clear();
            UnusedCards.Clear();

            foreach (CardM shuffledCard in newCardDeck)
                UnusedCards.Add(shuffledCard);

            UnusedCards.Shuffle();

            RaiseDeckShuffled();
        }

        private void RaiseDeckShuffled()
        {
            if (DeckShuffled != null)
                DeckShuffled(UnusedCards);
        }
    }
}

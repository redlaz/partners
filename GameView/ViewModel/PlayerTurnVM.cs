﻿using System.Windows.Media;
using GalaSoft.MvvmLight;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class PlayerTurnVM : ViewModelBase
    {
        private Color _color;
        public Color Color
        {
            get { return _color; }
            set { _color = value; RaisePropertyChanged("Color"); }
        }
        

        public int Number { get; set; }

        public PlayerTurnVM(PlayerM player)
        {
            Number = player.Number;
        }
    }
}

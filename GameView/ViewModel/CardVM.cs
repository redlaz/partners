﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class CardVM : ViewModelBase
    {
        public delegate void CardPickedDelegate(CardVM cardVM);
        public ICommand Picked { get; set; }
        public ICommand MouseEnter { get; set; }
        public CardM Card { get; set; }
        public string TitleHorizontal { get; set; }
        public string TitleVertical { get; set; }
        public string TitleCorner { get; set; }
        public CardPickedDelegate CardPicked { get; set; }

        public CardVM(CardM card)
        {
            Card = card;
            Picked = new RelayCommand(PickedExecute, () => true);
            ApplyValues();
        }

        private void ApplyValues()
        {
            if (Card is NumericCard)
            {
                var numericCard = Card as NumericCard;
                TitleCorner = TitleHorizontal = numericCard.Value.ToString();
            }

            if (Card is DoubleNumericCard)
            {
                var doubleNumericCard = Card as DoubleNumericCard;
                TitleVertical = TitleCorner = doubleNumericCard.Values[0] + "/" + doubleNumericCard.Values[1].ToString();
            }

            if (Card is SwapCard)
            {
                TitleVertical = "Swap";
            }

            if (Card is StartCard)
            {
                TitleCorner = "Start";
                TitleVertical = "Start";
            }

            if (Card is StartNumericCard)
            { 
                TitleVertical = "Start";
                TitleCorner = (Card as StartNumericCard).Value.ToString();
                TitleHorizontal = "";
            }

            if (Card is SplitableCard)
            {
                TitleHorizontal = "7";
                TitleCorner = "|| 7";
            }
        }

        private void PickedExecute()
        {
            Card.IsPicked = !Card.IsPicked;
        }
    }
}
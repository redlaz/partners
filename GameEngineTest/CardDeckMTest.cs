﻿using GameEngine.Exception;
using GameEngine.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameEngineTest
{
    [TestClass]
    public class CardDeckMTest
    {
        private const int NumOfCards = 48;

        [TestMethod]
        public void NumberOfCards()
        {
            var cardDeck = new CardDeckM();

            Assert.AreEqual(cardDeck.UsedCards.Count, 0);
            Assert.AreEqual(cardDeck.UnusedCards.Count, NumOfCards);

            cardDeck.DrawCard();
            Assert.AreEqual(cardDeck.UsedCards.Count, 0);
            Assert.AreEqual(cardDeck.UnusedCards.Count, NumOfCards - 1);
        }

        [TestMethod]
        [ExpectedException(typeof(DeckEmptyException))]
        public void DeckEmpty()
        {
            var cardDeck = new CardDeckM();
            for (int i = 0; i < NumOfCards + 1; i++)
                cardDeck.DrawCard();
        }

        [TestMethod]
        public void DeckCount()
        {
            var cardDeck = new CardDeckM();

            for (int i = 0; i < NumOfCards; i++)
            {
                Assert.AreEqual(i, cardDeck.UsedCards.Count);
                Assert.AreEqual(NumOfCards - i, cardDeck.UnusedCards.Count);

                var card = cardDeck.DrawCard();
                cardDeck.ReturnCard(card);
            }
        }
    }
}

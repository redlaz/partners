﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class StartVM
    {
        public int Angle { get; set; }
        public string Color { get; set; }
        public ObservableCollection<StartTileVM> Tiles { get; set; }
        public string HorizontalAlignment { get; set; }
        public string VerticalAlignment { get; set; }
        public Thickness Margin { get; set; }
        

        public StartVM(Color color, IEnumerable<StartTile> tiles)
        {
            Initialize();
            AppleSettings(color);
            ApplyTiles(tiles);
        }

        private void ApplyTiles(IEnumerable<StartTile> tiles)
        {
            foreach (var tile in tiles)
                Tiles.Add(new StartTileVM(tile));
        }

        private void AppleSettings(Color color)
        {
            Color = color.ToString();

            if (color == Colors.Blue)
            {
                Angle = 0;
                HorizontalAlignment = "Center";
                VerticalAlignment = "Bottom";
                Margin = new Thickness(0, 0, 0, 26);
            }

            if (color == Colors.Red)
            {
                Angle = -90;
                HorizontalAlignment = "Right";
                VerticalAlignment = "Center";
                Margin = new Thickness(0, 0, 36, 0);
            }

            if (color == Colors.Yellow)
            {
                Angle = 180;
                HorizontalAlignment = "Center";
                VerticalAlignment = "top";
                Margin = new Thickness(0, 26, 0, 0);
            }

            if (color == Colors.Green)
            {
                Angle = 90;
                HorizontalAlignment = "Left";
                VerticalAlignment = "Center";
                Margin = new Thickness(36, 0, 0, 0);
            }

        }

        private void Initialize()
        {
            Tiles = new ObservableCollection<StartTileVM>();
        }
    }
}

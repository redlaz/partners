﻿using GameEngine.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameEngineTest
{
    [TestClass]
    public class GameBoardMTest
    {
        private const int NumOfTiles = 60;

        [TestMethod]
        public void TilesCreated()
        {
            var gameBoard = new GameBoardM();
            TileM tile = gameBoard.BlueStartTiles[0];

            Assert.IsNotNull(tile);
            tile = tile.Next;
            Assert.IsTrue(tile is BoardTile);
            var boardTile = tile as BoardTile;

            for (int i = 0; i < NumOfTiles; i++)
            {
                Assert.IsNotNull(boardTile);

                Assert.AreEqual(i,boardTile.Number);

                if(boardTile.Number % 15 == 0)
                    Assert.IsNotNull(boardTile.HomeEntrance);
                
                boardTile = boardTile.Next as BoardTile;
            }
        }
    }
}

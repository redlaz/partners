﻿using System.Windows.Media;
using GalaSoft.MvvmLight;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class PieceVM : ViewModelBase
    {
        public SolidColorBrush Color { get; set; }
        public PieceM Piece { get; set; }

        private bool _isOntile;
        public bool IsOnTile
        {
            get { return _isOntile; }
            set { _isOntile = value; RaisePropertyChanged("IsOnTile"); }
        }
        
        public PieceVM(PieceM piece)
        {
            Piece = piece;
            Color = new SolidColorBrush(piece.Color);
        }
    }
}

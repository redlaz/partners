﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GameEngine.Model
{
    public class GameBoardM
    {
        private const int NumOfTiles = 60;
        private const int BlueStart = 0;
        private const int GreenStart = 45;
        private const int YellowStart = 30;
        private const int RedStart = 15;
        private const int NumOfHomes = 4, NumOfStartTiles = 4;

        public List<BoardTile> Tiles { get; set; }
        public List<StartTile> BlueStartTiles { get; set; }
        public List<StartTile> GreenStartTiles { get; set; }
        public List<StartTile> YellowStartTiles { get; set; }
        public List<StartTile> RedStartTiles { get; set; }
        public List<HomeTile> BlueHomeTiles { get; set; }
        public List<HomeTile> GreenHomeTiles { get; set; }
        public List<HomeTile> YellowHomeTiles { get; set; }
        public List<HomeTile> RedHomeTiles { get; set; }

        public GameBoardM()
        {
            Initialize();
            CreateHomeTiles();
            CreateTiles();
            CreateStartTiles();
        }

        private void CreateHomeTiles()
        {
            for (int i = 0; i < NumOfHomes; i++)
            {
                BlueHomeTiles.Add(new HomeTile());
                GreenHomeTiles.Add(new HomeTile());
                YellowHomeTiles.Add(new HomeTile());
                RedHomeTiles.Add(new HomeTile());
            }

            for (int i = 0; i < NumOfHomes; i++)
            {
                if (i < NumOfStartTiles - 1)
                {
                    BlueHomeTiles[i].Next = BlueHomeTiles[i + 1];
                    GreenHomeTiles[i].Next = GreenHomeTiles[i + 1];
                    RedHomeTiles[i].Next = RedHomeTiles[i + 1];
                    YellowHomeTiles[i].Next = YellowHomeTiles[i + 1];
                }
            }
        }

        private void Initialize()
        {
            Tiles = new List<BoardTile>();
            BlueStartTiles = new List<StartTile>();
            GreenStartTiles = new List<StartTile>();
            YellowStartTiles = new List<StartTile>();
            RedStartTiles = new List<StartTile>();
            BlueHomeTiles = new List<HomeTile>();
            GreenHomeTiles = new List<HomeTile>();
            YellowHomeTiles = new List<HomeTile>();
            RedHomeTiles = new List<HomeTile>();
        }

        private void CreateStartTiles()
        {
            for (int i = 0; i < NumOfStartTiles; i++)
            {
                BlueStartTiles.Add(new StartTile { Next = Tiles[BlueStart] });
                GreenStartTiles.Add(new StartTile { Next = Tiles[GreenStart] });
                YellowStartTiles.Add(new StartTile { Next = Tiles[YellowStart] });
                RedStartTiles.Add(new StartTile { Next = Tiles[RedStart] });
            }
        }

        public void CreateTiles()
        {
            var firstTile = new BoardTile();

            for (int i = 0; i < NumOfTiles; i++)
            {
                var tile = new BoardTile
                {
                    Number = i,
                    Pieces = new ObservableCollection<PieceM>()
                };

                if (i == BlueStart)
                    tile.HomeEntrance = BlueHomeTiles[0];

                if (i == RedStart)
                    tile.HomeEntrance = RedHomeTiles[0];

                if (i == YellowStart)
                    tile.HomeEntrance = BlueHomeTiles[0];

                if (i == GreenStart)
                    tile.HomeEntrance = GreenHomeTiles[0];
	            
                if (i == 0)
                    firstTile = tile;

                else if (i == NumOfTiles - 1)
                {
                    var prevTile = Tiles[Tiles.Count - 1];

                    prevTile.Next = tile;
                    tile.Prev = prevTile;
                    tile.Next = firstTile;
                    firstTile.Prev = tile;
                }

                else
                {
                    var prevTile = Tiles[Tiles.Count - 1];
                    tile.Prev = prevTile;
                    prevTile.Next = tile;
                }

                Tiles.Add(tile);
            }
        }
    }
}

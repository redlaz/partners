﻿using GameEngine.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameEngineTest
{
    [TestClass]
    public class CardMTest
    {
        private static int _eventCount;

        [TestMethod]
        public void EventRaised()
        {
            CardM card1 = new DoubleNumericCard();
            CardM card2 = new DoubleNumericCard();

            card1.Picked += Picked;
            card2.Picked += Picked;

            card1.IsPicked = true;
            card2.IsPicked = true;

            Assert.AreEqual(2, _eventCount);
        }

        private void Picked(CardM card)
        {
            _eventCount++;
        }
    }
}

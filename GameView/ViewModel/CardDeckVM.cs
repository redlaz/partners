﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class CardDeckVM
    {
        private CardDeckM CardDeck { get; set; }

        public ObservableCollection<CardVM> Unused{ get; set; }
        public ObservableCollection<CardVM> Used { get; set; }

        public CardDeckVM(CardDeckM cardDeck)
        {
            CardDeck = cardDeck;
            Initialize();
            ApplyCards();
        }

        private void ApplyCards()
        {
            foreach (CardM card in CardDeck.UnusedCards)
                Unused.Add(new CardVM(card));
        }

        private void Initialize()
        {
            Used = new ObservableCollection<CardVM>();
            Unused = new ObservableCollection<CardVM>();

            CardDeck.CardDrawn += CardDrawn;
            CardDeck.CardReturned += CardReturned;
            CardDeck.DeckShuffled += DeckShuffled;
        }

        private void DeckShuffled(List<CardM> cards)
        {
            Used.Clear();
            Unused.Clear();

            foreach (CardM card in cards)
                Unused.Add(new CardVM(card));
        }

        private void CardReturned(CardM card)
        {
            Used.Add(new CardVM(card));
        }

        private void CardDrawn(CardM card)
        {
            CardVM cardToBeRemoved = Unused.First(unusedCard => unusedCard.Card == card);
            Unused.Remove(cardToBeRemoved);
        }
    }
}

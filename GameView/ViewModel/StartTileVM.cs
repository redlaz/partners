﻿using GameEngine.Model;

namespace GameView.ViewModel
{
    public class StartTileVM
    {
        public StartTile HomeTile { get; set; }

        public StartTileVM(StartTile homeTile)
        {
            HomeTile = homeTile;
        }
    }
}

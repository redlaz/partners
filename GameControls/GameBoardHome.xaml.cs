﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameControls
{
	/// <summary>
	/// Interaction logic for GameBoardHome.xaml
	/// </summary>
	public partial class GameBoardHome : UserControl
	{
        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(string), typeof(GameBoardHome),null);

        
		public GameBoardHome()
		{
			this.InitializeComponent();
		}
	}
}
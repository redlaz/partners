﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameControls
{
	/// <summary>
	/// Interaction logic for GameBoardSquare.xaml
	/// </summary>
	public partial class GameBoardSquare : UserControl
	{
        public GameBoardSquare NextSquare { get; set; }
        public int Number { get; set; }

        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =  DependencyProperty.Register("Color", typeof(string), typeof(GameBoardSquare), null);

		public GameBoardSquare()
		{
			this.InitializeComponent();
		}
	}
}
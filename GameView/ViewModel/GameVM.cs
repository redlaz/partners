﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class GameVM : ViewModelBase
    {
        public GameM Game { get; set; }
        public GameBoardVM GameBoard { get; set; }
        public CardDeckVM CardDeck { get; set; }
        public ObservableCollection<MessageVM> Messages { get; set; }
        public ObservableCollection<PlayerVM> Players { get; set; }
        public ObservableCollection<PlayerTurnVM> PlayerTurn { get; set; }
        
        private int _computerProgress;
        public int ComputerProgress
        {
            get { return _computerProgress; }
            set { _computerProgress = value; RaisePropertyChanged("ComputerProgress"); }
        }
        

        public ICommand StartGame { get; set; }

        public GameVM()
        {
            Game = new GameM();
            Initialize();
            ApplyEventHandlers();

            foreach (PlayerM player in Game.Players)
                PlayerTurn.Add(new PlayerTurnVM(player));

            SetRelayCommand();
            ApplyPlayers();
            
        }

        private void ApplyEventHandlers()
        {
            Game.PlayerSwitched += PlayerSwitched;
            Game.Message += MessageReceivedFromGame;
            Game.ComputerProgress += ComputerProgressChanged;
        }

        private void ApplyPlayers()
        {
            foreach (PlayerM player in Game.Players)
                Players.Add(new PlayerVM(player));
        }

        private void SetRelayCommand()
        {
            StartGame = new RelayCommand(Start, () => true);
        }

        private void MessageReceivedFromGame(string message)
        {
            Messages.Clear();
            Messages.Add(new MessageVM { Message = message });
        }

        private void Start()
        {
            Game.StartGame();
        }

        private void Initialize()
        {
            CardDeck = new CardDeckVM(Game.Deck);
            Messages = new ObservableCollection<MessageVM>();
            GameBoard = new GameBoardVM(Game.GameBoard);
            PlayerTurn = new ObservableCollection<PlayerTurnVM>();
            Players = new ObservableCollection<PlayerVM>();
        }

        private void ComputerProgressChanged(int percent)
        {
            ComputerProgress = percent;
        }

        private void PlayerSwitched(PlayerM player)
        {
            foreach (PlayerTurnVM playerturnVM in PlayerTurn)
            {
                playerturnVM.Color = Colors.Gray;

                if (playerturnVM.Number == player.Number)
                    playerturnVM.Color = player.Color;
            }
        }
    }
}

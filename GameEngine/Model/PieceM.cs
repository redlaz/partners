﻿using System.Collections.Generic;
using System.Windows.Media;

namespace GameEngine.Model
{
    public class PieceM
    {
        public TileM Tile { get; set; }
        public TileM HomeTile { get; set; }
        public Color Color { get; set; }
        public List<TileM> ReachableTiles { get; set; }

        public PieceM(TileM hometile, Color color)
        {
            Initialize();

            Tile = hometile;
            HomeTile = hometile;
            Color = color;
        }

        private void Initialize()
        {
            ReachableTiles = new List<TileM>();
        }

        public void GoToHome()
        {
            HomeTile.Enter(this);
        }

        public bool IsSameColor(PieceM piece)
        {
            return Color == piece.Color;
        }
    }
}

﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GameEngine.Model;

namespace GameView.ViewModel
{
    public class BoardTileVM : ViewModelBase
    {
        public TileM Tile { get; set; }
        public int Angle { get; set; }
        
        public ObservableCollection<PieceVM> Pieces { get; set; }
        private bool _marked;
        public bool Marked
        {
            get { return _marked; }
            set { _marked = value; RaisePropertyChanged("Marked"); }
        }

        private SolidColorBrush _color;
        public SolidColorBrush Color
        {
            get { return _color; }
            set { _color = value; RaisePropertyChanged("Color"); }
        }

        public Point Position { get; set; }
        public double Scale { get; set; }

        public ICommand Picked { get; set; }

        public BoardTileVM(BoardTile tile)
        {
            Initialize(tile);

            Angle = -tile.Number * 6;
            Color = new SolidColorBrush(GetColor(tile.Number));
            Scale = 0.8;
        }

        private void Initialize(BoardTile tile)
        {
            Pieces = new ObservableCollection<PieceVM>();
            Picked = new RelayCommand(PickedExecute, () => true);
            Tile = tile;
            Tile.Marked += TileMarked;
            Tile.PieceEntered += PieceEntered;
            Tile.PieceLeft += PieceLeft;
        }

        private async void PieceLeft(PieceM piece)
        {
            var pieceVM = Pieces.First(p => p.Piece == piece);
            pieceVM.IsOnTile = false;
            await Task.Delay(1000); // Wait for "leaving" animation before removing
            Pieces.Remove(pieceVM);
        }

        private async void PieceEntered(PieceM piece)
        {
            await Task.Delay(500); // Wait for "leaving" animation
            PieceVM pieceVM = new PieceVM(piece);
            pieceVM.IsOnTile = true;
            Pieces.Add(pieceVM);
        }

        private void PickedExecute()
        {
            Tile.Pick();
        }

        private void TileMarked(TileM tile)
        {
            Marked = tile.IsMarked;
        }


        private Color GetColor(int tileNumber)
        {
            switch (tileNumber % 4)
            {
                case 3:
                    return Colors.Red;

                case 2:
                    return Colors.Yellow;

                case 1:
                    return Colors.Green;

                default:
                    return Colors.Blue;
            }
        }
    }
}
